﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Largest_Prime_Factor
{
    public partial class MainWindow : Window
    {
        //Initalise variables for FOR loop
        long originalNum = 600851475143, numBeingFactored = 0, primeFactor = 0;
        public MainWindow()
        {
            InitializeComponent();
            //Write basic instructions to lable on window
            lblInstruction.Content = "Click to find the highest Prime Factor of " + originalNum;
        }

        private void btnClick_Click(object sender, RoutedEventArgs e)
        {
            int division = 2, factor = 0;
            numBeingFactored = originalNum;

            do
            {
                if(numBeingFactored % division == 0)
                {
                    factor = division;
                    numBeingFactored = numBeingFactored / division;
                }

                division++;
            }
            while (numBeingFactored != 1);

            lblAnswer.Content = "The highest prime factor of " + originalNum + " is: " + factor;
        }
    }
}
